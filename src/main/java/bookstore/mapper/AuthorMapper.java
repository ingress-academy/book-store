package bookstore.mapper;

import bookstore.dto.request.CreateAuthorRequest;
import bookstore.dto.response.AuthorResponse;
import bookstore.entity.AuthorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface AuthorMapper {

    @Mapping(target = "id", ignore = true)
    AuthorEntity mapAuthorRequestToAuthorEntity(CreateAuthorRequest authorRequest);

    AuthorResponse mapAuthorEntityToAuthorResponse(AuthorEntity authorEntity);
}
