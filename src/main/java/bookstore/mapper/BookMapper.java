package bookstore.mapper;

import bookstore.dto.request.CreateBookRequest;
import bookstore.dto.response.BookResponse;
import bookstore.entity.BookEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface BookMapper {

    @Mapping(target = "id", ignore = true)
    BookEntity mapBookRequestToBookEntity(CreateBookRequest createBookRequest);

    BookResponse mapBookEntitytoBookResponse(BookEntity bookEntity);
}
