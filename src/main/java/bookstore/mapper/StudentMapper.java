package bookstore.mapper;


import bookstore.dto.request.CreateStudentRequest;
import bookstore.dto.request.UpdateStudentRequest;
import bookstore.dto.response.StudentResponse;
import bookstore.entity.StudentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface StudentMapper {

    @Mapping(target = "id", ignore = true)
    StudentEntity mapStudentRequestToStudentEntity(CreateStudentRequest request);

    StudentResponse mapStudentEntityToStudentResponse(StudentEntity studentEntity);

    StudentEntity mapUpdateRequestToStudentEntity(@MappingTarget StudentEntity student, UpdateStudentRequest request);
}
