package bookstore.mapper;

import bookstore.dto.request.ReaderRequest;
import bookstore.entity.ReaderEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ReaderMapper {

    @Mapping(target = "id", ignore = true)
    ReaderEntity mapReaderRequestToReaderEntity(ReaderRequest request);
}
