package bookstore.mapper;

import bookstore.dto.request.SubscriberRequest;
import bookstore.entity.AuthorEntity;
import bookstore.entity.BookEntity;
import bookstore.entity.StudentEntity;
import bookstore.entity.SubscriberEntity;
import bookstore.repository.AuthorRepository;
import bookstore.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomMapper {

    private final AuthorRepository authorRepository;
    private final StudentRepository studentRepository;

    public AuthorEntity mapOptionalToAuthorEntity(Optional<AuthorEntity> optionalAuthorEntity) {

        if (optionalAuthorEntity.isEmpty()) {
            return null;
        }

        return optionalAuthorEntity.orElse(new AuthorEntity());
    }

    public StudentEntity mapOptionalToStudentEntity(Optional<StudentEntity> optionalStudent) {

        if (optionalStudent.isEmpty()) {
            return null;
        }

        return optionalStudent.orElse(new StudentEntity());
    }

    public BookEntity mapOptionalToBookEntity(Optional<BookEntity> optionalBookEntity) {

        if(optionalBookEntity.isEmpty()) {
            return null;
        }

        return optionalBookEntity.orElse(new BookEntity());
    }

    public SubscriberEntity mapSubscriberRequestToSubscriberEntity(SubscriberRequest subscriberRequest) {
        SubscriberEntity subscriberEntity = new SubscriberEntity();
        AuthorEntity author = mapOptionalToAuthorEntity(authorRepository.findById(subscriberRequest.getAuthorId()));
        StudentEntity student = mapOptionalToStudentEntity(studentRepository.findById(subscriberRequest.getStudentId()));

        subscriberEntity.setAuthor(author);
        subscriberEntity.setStudent(student);

        return subscriberEntity;
    }
}
