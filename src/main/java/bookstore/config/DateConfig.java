package bookstore.config;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateConfig {

    public static LocalDateTime convertFromStringToLocalDateTime(String str) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        return LocalDateTime.parse(str, formatter);
    }

    public static String convertFromLocalDateTimeToString(LocalDateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        return dateTime.format(formatter);
    }

    public static String now() {
        LocalDateTime loc = LocalDateTime.now();

        return convertFromLocalDateTimeToString(loc);
    }
}
