package bookstore.repository;


import bookstore.entity.Token;
import bookstore.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<Token,Long> {
    Optional<Token> findTokenByToken(String token);
    Optional<Token> findTokenByUser(UserEntity user);
}
