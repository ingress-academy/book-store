package bookstore.repository;


import bookstore.entity.ReaderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ReaderRepository extends JpaRepository<ReaderEntity, Long> {

    @Modifying
    @Query(value = "UPDATE readers " +
            "SET last_reading_time = :lastReadingTime " +
            "WHERE book_id = :bookId AND student_id = :studentId", nativeQuery = true)
    void updateLastReadingTime(@Param("bookId") Long bookId, @Param("studentId") Long studentId, @Param("lastReadingTime") String lastReadingTime);

    @Query(value = "SELECT r.last_reading_time FROM readers r WHERE r.book_id = :bookId AND r.student_id = :studentId", nativeQuery = true)
    String findLastReadingTimeByBookAndStudent(@Param("bookId") Long bookId, @Param("studentId") Long studentId);

    @Query(value = "SELECT books.name\n" +
            "FROM books\n" +
            "JOIN book_store_app.readers r on books.id = r.book_id\n" +
            "WHERE STR_TO_DATE(r.last_reading_time, '%Y-%m-%d %H:%i') >= DATE_SUB(NOW(), INTERVAL 5 MINUTE)\n" +
            "AND STR_TO_DATE(r.last_reading_time,  '%Y-%m-%d %H:%i') <= DATE_ADD(NOW(), INTERVAL 5 MINUTE) AND student_id = :studentId", nativeQuery = true)
    List<String> currentReadingBookList(Long studentId);
}
