package bookstore.repository;

import bookstore.entity.SubscriberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SubscriberRepository extends JpaRepository<SubscriberEntity, Long> {

        @Query(value = "SELECT email \n" +
                "FROM subscribers\n" +
                "INNER JOIN students\n" +
                "ON subscribers.student_id=students.id\n" +
                "WHERE author_id = :authorId"  , nativeQuery = true)
        List<String> findSubscribersEmailByAuthorId(Long authorId);
}
