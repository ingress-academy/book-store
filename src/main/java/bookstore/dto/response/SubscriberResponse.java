package bookstore.dto.response;

import bookstore.entity.AuthorEntity;
import bookstore.entity.StudentEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SubscriberResponse {

    Long id;
    AuthorEntity author;
    StudentEntity student;
}
