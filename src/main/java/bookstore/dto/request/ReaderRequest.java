package bookstore.dto.request;

import bookstore.entity.BookEntity;
import bookstore.entity.StudentEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReaderRequest {

    BookEntity book;
    StudentEntity student;
    String lastReadingTime;

}
