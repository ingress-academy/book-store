package bookstore.controller;

import bookstore.dto.request.SubscriberRequest;
import bookstore.service.SubscriberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class SubscriberController {

    private final SubscriberService subscriberService;

    @PostMapping("/follow")
    public void followAuthor(@RequestHeader("id") Long userId, @RequestBody SubscriberRequest request) {
        subscriberService.addSubscriberToAuthor(userId,request);
    }
}
