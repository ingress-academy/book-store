package bookstore.controller;

import bookstore.dto.response.BookResponse;
import bookstore.repository.ReaderRepository;
import bookstore.service.ReaderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class ReaderController {
    private final ReaderRepository readerRepository;

    private final ReaderService readerService;

    @PostMapping("/read/{id}")
    public void createReaderForSpesificBook(@PathVariable(name = "id") Long id ,@RequestHeader("id") Long studentId ) {
       if (readerService.isThisForUpdate(id, studentId)) {
           readerService.updateLastReadingTime(id, studentId);
       }else {
           readerService.createReader(id,studentId);
       }
    }

    @GetMapping("/current")
    public List<String> currentReadingBookList(@RequestHeader("id") Long studentId){
        return readerRepository.currentReadingBookList(studentId);
    }
}
