package bookstore.controller;

import bookstore.dto.request.CreateBookRequest;
import bookstore.dto.response.BookResponse;
import bookstore.service.BookService;
import bookstore.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;
    private final MailService mailService;

    @PostMapping
    public void createBookAndSendMailForFollowers(@RequestHeader("id") Long id, @RequestBody CreateBookRequest bookRequest) {
        bookService.createBook(id, bookRequest);
        mailService.sendMailForAddingBookToAuthorsFollowers(bookRequest.getAuthor());
    }

    @GetMapping
    public List<BookResponse> getBooks() {
       return bookService.getBooks();
    }
}
