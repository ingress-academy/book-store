package bookstore.controller;

import bookstore.dto.response.AuthorResponse;
import bookstore.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping
    public List<AuthorResponse> getAuthors() {
        return authorService.getAuthors();
    }
}
