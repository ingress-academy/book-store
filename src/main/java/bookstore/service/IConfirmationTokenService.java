package bookstore.service;


import bookstore.dto.response.ResponseDto;
import bookstore.entity.ConfirmationToken;

public interface IConfirmationTokenService {
    ResponseDto save(ConfirmationToken confirmationToken);
    ConfirmationToken getTokenByUUID(String uuid);
}