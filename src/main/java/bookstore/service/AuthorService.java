package bookstore.service;

import bookstore.dto.request.CreateAuthorRequest;
import bookstore.dto.response.AuthorResponse;

import java.util.List;

public interface AuthorService {

    void createAuthor(CreateAuthorRequest authorRequest);
    List<AuthorResponse> getAuthors();

}
