package bookstore.service;

import bookstore.entity.AuthorEntity;

import java.util.List;

public interface MailService {

    void sendMailForAddingBookToAuthorsFollowers(AuthorEntity author);
}
