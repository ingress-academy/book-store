package bookstore.service;

import bookstore.dto.request.SubscriberRequest;

public interface SubscriberService {

    void addSubscriberToAuthor(Long userId, SubscriberRequest subscriberRequest);
}
