package bookstore.service;

import bookstore.dto.request.CreateStudentRequest;
import bookstore.dto.request.UpdateStudentRequest;
import bookstore.dto.response.StudentResponse;

import java.util.List;

public interface StudentService {
    void createStudent(CreateStudentRequest request);

    List<StudentResponse> getAllStudents();

    StudentResponse getStudent(Long id);

    void updateStudent(Long id, UpdateStudentRequest request);

    void deleteStudent(Long id);
}
