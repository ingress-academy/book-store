package bookstore.service;

import bookstore.dto.request.CreateBookRequest;
import bookstore.dto.response.BookResponse;

import java.util.List;

public interface BookService {

    void createBook(Long userId, CreateBookRequest bookRequest);
    List<BookResponse> getBooks();


}
