package bookstore.service.impl;


import bookstore.dto.response.ResponseDto;
import bookstore.entity.ConfirmationToken;
import bookstore.enums.Exceptions;
import bookstore.exception.ApplicationException;
import bookstore.repository.ConfirmationTokenRepository;
import bookstore.service.IConfirmationTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;



@Service
@RequiredArgsConstructor
public class ConfirmationTokenService implements IConfirmationTokenService {
    private final ConfirmationTokenRepository confirmationTokenRepository;

    @Override
    public ResponseDto save(ConfirmationToken confirmationToken) {
        ConfirmationToken save = confirmationTokenRepository.save(confirmationToken);
        if (save != null){
            return new ResponseDto("Save is successfull");
        } else {
            throw new ApplicationException(Exceptions.TOKEN_IS_INVALID_EXCEPTION);
        }
    }

    @Override
    public ConfirmationToken getTokenByUUID(String uuid) {
        return confirmationTokenRepository.findConfirmationTokenByToken(uuid)
                .orElseThrow(() -> new ApplicationException(Exceptions.TOKEN_NOT_FOUND_EXCEPTION));
    }
}