package bookstore.service.impl.security;


import bookstore.dto.request.AuthenticationRequest;
import bookstore.dto.request.RegistrationRequest;
import bookstore.dto.response.AuthenticationResponse;

public interface IAuthService {
    AuthenticationResponse registration(RegistrationRequest request);
    AuthenticationResponse authentication(AuthenticationRequest request);
    AuthenticationResponse refreshToken(String authHeader);

}
