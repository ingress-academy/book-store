package bookstore.service.impl;

import bookstore.config.DateConfig;
import bookstore.dto.request.ReaderRequest;
import bookstore.entity.BookEntity;
import bookstore.entity.StudentEntity;
import bookstore.mapper.CustomMapper;
import bookstore.mapper.ReaderMapper;
import bookstore.repository.BookRepository;
import bookstore.repository.ReaderRepository;
import bookstore.repository.StudentRepository;
import bookstore.service.ReaderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ReaderServiceImpl implements ReaderService {

    private final ReaderRepository readerRepository;
    private final BookRepository bookRepository;
    private final StudentRepository studentRepository;

    private final ReaderMapper readerMapper;
    private final CustomMapper customMapper;

    @Override
    public void createReader(Long bookId, Long studentId) {

        StudentEntity student = customMapper.mapOptionalToStudentEntity(studentRepository.findById(studentId));
        BookEntity book = customMapper.mapOptionalToBookEntity(bookRepository.findById(bookId));
        String lastReadingTime = DateConfig.now();
        ReaderRequest request = new ReaderRequest(book, student, lastReadingTime);
        readerRepository.save(readerMapper.mapReaderRequestToReaderEntity(request));

        log.info("{} -> readers created ", book.getName());
    }

    @Override
    @Transactional
    public void updateLastReadingTime(Long bookId, Long studentId) {
        String lastReadingTime = DateConfig.now();
        readerRepository.updateLastReadingTime(bookId, studentId, lastReadingTime);
    }

    @Override
    public boolean isThisForUpdate(Long bookId, Long studentId) {
        return readerRepository.findLastReadingTimeByBookAndStudent(bookId, studentId) != null;
    }

    @Override
    public List<String> currentReadingBookList(Long studentId) {

        return readerRepository.currentReadingBookList(studentId);
    }
}
