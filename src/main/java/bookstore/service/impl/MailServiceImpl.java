package bookstore.service.impl;

import bookstore.entity.AuthorEntity;
import bookstore.model.MailStructure;
import bookstore.repository.SubscriberRepository;
import bookstore.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private final JavaMailSender mailSender;
    private final SubscriberRepository subscriberRepository;

    @Value("${spring.mail.username}")
    private String fromMail;

    @Override
    public void sendMailForAddingBookToAuthorsFollowers( AuthorEntity author) {

       List<String> emails = subscriberRepository.findSubscribersEmailByAuthorId(author.getId());
        for (String email: emails) {

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom(fromMail);
            MailStructure mailStructure = new MailStructure();
            mailStructure.setSubject("BOOKSTORE");
            mailStructure.setMessage(author.getName() + " -in yeni kitabi elave olundu.");
            mailMessage.setSubject(mailStructure.getSubject());
            mailMessage.setText(mailStructure.getMessage());
            mailMessage.setTo(email);

            mailSender.send(mailMessage);
        }
    }
}
