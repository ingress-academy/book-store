package bookstore.service.impl;

import bookstore.dto.request.SubscriberRequest;
import bookstore.mapper.CustomMapper;
import bookstore.repository.SubscriberRepository;
import bookstore.service.SubscriberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class SubscriberServiceImpl implements SubscriberService {

    private final SubscriberRepository subscriberRepository;
    private final CustomMapper customMapper;

    @Override
    public void addSubscriberToAuthor(Long userId, SubscriberRequest request) {
        request.setStudentId(userId);
        subscriberRepository.save(customMapper.mapSubscriberRequestToSubscriberEntity(request));
    }
}
