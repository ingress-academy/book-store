package bookstore.service.impl;

import bookstore.dto.request.CreateAuthorRequest;
import bookstore.dto.response.AuthorResponse;
import bookstore.mapper.AuthorMapper;
import bookstore.repository.AuthorRepository;
import bookstore.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    @Override
    public void createAuthor(CreateAuthorRequest authorRequest) {
        authorRepository.save(authorMapper.mapAuthorRequestToAuthorEntity(authorRequest));
    }

    @Override
    public List<AuthorResponse> getAuthors() {
        return authorRepository.findAll()
                .stream().map(authorMapper::mapAuthorEntityToAuthorResponse).collect(Collectors.toList());
    }
}
