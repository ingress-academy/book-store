package bookstore.service.impl;

import bookstore.dto.request.CreateBookRequest;
import bookstore.dto.response.BookResponse;
import bookstore.entity.AuthorEntity;
import bookstore.mapper.BookMapper;
import bookstore.mapper.CustomMapper;
import bookstore.model.MailStructure;
import bookstore.repository.AuthorRepository;
import bookstore.repository.BookRepository;
import bookstore.repository.SubscriberRepository;
import bookstore.service.BookService;
import bookstore.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final BookMapper bookMapper;
    private final CustomMapper customMapper;
    private final AuthorRepository authorRepository;

    @Override
    public void createBook(Long userId, CreateBookRequest bookRequest) {
        AuthorEntity author = customMapper.mapOptionalToAuthorEntity(authorRepository.findById(userId));
        bookRequest.setAuthor(author);
        bookRepository.save(bookMapper.mapBookRequestToBookEntity(bookRequest));
    }

    @Override
    public List<BookResponse> getBooks() {
        return bookRepository.findAll()
                .stream().map(bookMapper::mapBookEntitytoBookResponse)
                .collect(Collectors.toList());
    }
}
