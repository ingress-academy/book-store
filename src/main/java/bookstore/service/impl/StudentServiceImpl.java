package bookstore.service.impl;


import bookstore.dto.request.CreateStudentRequest;
import bookstore.dto.request.UpdateStudentRequest;
import bookstore.dto.response.StudentResponse;
import bookstore.exception.StudentNotFoundException;
import bookstore.mapper.StudentMapper;
import bookstore.repository.AuthorRepository;
import bookstore.repository.StudentRepository;
import bookstore.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;
    private final AuthorRepository authorRepository;


    @Override
    public void createStudent(CreateStudentRequest request) {
        studentRepository.save(studentMapper.mapStudentRequestToStudentEntity(request));
    }

    @Override
    public List<StudentResponse> getAllStudents() {
        return studentRepository.findAll()
                .stream()
                .map(studentMapper::mapStudentEntityToStudentResponse)
                .collect(Collectors.toList());
    }

    @Override
    public StudentResponse getStudent(Long id) {
        var student = studentRepository.findById(id)
                .orElseThrow(() -> exStudentNotFound(id));

        return studentMapper.mapStudentEntityToStudentResponse(student);
    }

    @Override
    public void updateStudent(Long id, UpdateStudentRequest request) {
        var student = studentRepository.findById(id)
                .orElseThrow(() -> exStudentNotFound(id));

        studentMapper.mapUpdateRequestToStudentEntity(student, request);
    }

    @Override
    public void deleteStudent(Long id) {
        var student = studentRepository.findById(id)
                .orElseThrow(() -> exStudentNotFound(id));
    }

    public StudentNotFoundException exStudentNotFound(Long id) {
        throw new StudentNotFoundException("Student not found by id" + id);
    }
}
