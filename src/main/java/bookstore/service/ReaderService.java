package bookstore.service;

import bookstore.dto.request.ReaderRequest;
import bookstore.dto.response.BookResponse;

import java.util.List;

public interface ReaderService {
    void createReader(Long bookId, Long studentId);
    void updateLastReadingTime(Long bookId, Long studentId);
    boolean isThisForUpdate(Long bookId, Long studentId);
    List<String> currentReadingBookList(Long studentId);
}
